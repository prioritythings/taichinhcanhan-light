const path = require('path')
const src = path.resolve(__dirname, 'src')
const dist = path.resolve(__dirname, 'dist')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = {
	entry: {
		index: path.resolve(src, 'index.js')
	},
	output: {
		filename: '[name].js',
		path: dist
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				styles: {
					name: 'styles',
					test: /\.css$/,
					chunks: 'all',
					enforce: true
				}
			}
		}
	},
	plugins: [
		new CleanWebpackPlugin([dist]),
		new HtmlWebpackPlugin({
			template: path.resolve(src, 'index.html'),
			filename: 'index.html',
			inject: 'body'
		}),
		new MiniCssExtractPlugin({
			filename: "[name].css",
		})
	],
	module: {
		rules: [
			{
				test: /\.(sc|sa|c)ss/,
				use: [
					MiniCssExtractPlugin.loader, // replace style-loader
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							includePaths: [path.resolve(__dirname, 'src/sass')]
						}
					}
				]
			}
		]
	}
}