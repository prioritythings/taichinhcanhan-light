const express = require('express')
const router = express.Router()
const databaseControler = require('../controllers/databaseController')

router.get('/', databaseControler.database_list)

router.get('/:name', databaseControler.database_detail)

module.exports = router