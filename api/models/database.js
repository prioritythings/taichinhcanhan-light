var pool = require('../core/db')
const databases = [
    "mysql",
    "redis",
    "elastic-search",
    "postgresql",
    "mongodb"
]
class Database {
    async find(filters = {}) {
        let rows = await pool.query('SELECT * FROM database_systems;')
        console.log(rows)
        return rows
    }
    findByName(name) {
        let id = databases.indexOf(name)
        console.log(id, databases[id])
        return id >= 0 ? databases[id] : null
    }
}

module.exports = new Database()