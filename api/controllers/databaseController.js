const Database = require('../models/database')
const async = require('async')

exports.database_list = async (req, res, next) => {
    let database_list = await Database.find()
    res.status(200).json({
        database_list: database_list,
        integrations: "https://expressjs.com/en/guide/database-integration.html#mongodb"
    })
}

exports.database_detail = (req, res, next) => {
    async.parallel({
        database: (callback) => {
            callback(null, Database.findByName(req.params.name))
        }
    }, (err, results) => {
        console.log(results)
        if (err) { return next(err) }
        if (results.database == null) {// no results
            let err = new Error('Database not found!')
            err.status = 404
            return next(err)
        }
        // successfull
        res.status(200).json({
            'database': results.database
        })
    })
}