# General

- Express generator : https://github.com/yarnpkg/yarn/issues/5167

```bash
yarn global add express-generator
express api --view=pug --css=css --git -f

yarn start
```

- https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/mongoose
- https://expressjs.com/en/4x/api.html#router
- https://github.com/mdn/express-locallibrary-tutorial
- http://2ality.com/2014/09/es6-modules-final.html
- https://fullstack-developer.academy/unexpected-token-export-error/
- https://caolan.github.io/async/docs.html#parallel
- https://www.digitalocean.com/community/tutorials/how-to-use-winston-to-log-node-js-applications
- https://codingsans.com/blog/node-config-best-practices
- https://codeburst.io/node-js-mysql-and-promises-4c3be599909b
- https://www.w3schools.com/nodejs/nodejs_mysql.asp